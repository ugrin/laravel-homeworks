<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
 	{{ csrf_field() }}
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3" style="margin-top: 25px">	
	 	@foreach($users as $user)
	 		<ul style="font-size: 16px">
	 			<li>{{$user->name}} --- {{$user->votes}} --- {{$user->is_active}} </li>
	 			<a href="{{route('update',$user->id)}}">Update</a>
	 			<a href="{{route('delete',$user->id)}}">Delete</a> 			
	 		</ul>
 		@endforeach

 		{{$users->links()}}
 		</div>
 	</div>
</div>

</body>
</html>


