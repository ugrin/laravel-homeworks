<!DOCTYPE html>
<html>
<head>
	<title>Update User</title>
		<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h1 style="font-size: 32px" class="text-center">Update user</h1>
		</div>
	</div>
	<div class="row">
	 	<div class="col-md-4 col-md-offset-4" style="margin-top: 25px">
		 	@foreach($users as $user) 
			<form  class="form-horizontal" role="form" action="{{route('update', $user->id)}}" method="POST">
		 	{{ csrf_field() }}

			<label>Внесете го Вашето корисничко име</label>
			<input type="text" name="username" class="form-control" value="{{$user->name}}">

			<label>Внесете го вашиот глас</label>
			<input type="text" name="vote" class="form-control" value="{{$user->votes}}">

			<label>Is active</label>
			<select class="form-control" name="is_active" value="$user->is_active">
				<option>Yes</option>
				<option>No</option>				
			</select>
			
			@endforeach
			<br>

			<input type="submit" value="Потврди" class="form-control submit">

			</form>
		</div>
	</div>		

	</form>

</body>
</html>