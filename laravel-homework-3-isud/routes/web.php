<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/show-form')->name('show')->uses('UserController@showForm');

Route::post('/insert-users')->name('insert')->uses('UserController@insertUser');

Route::get('/get-users')->name('get-users')->uses('UserController@getUsers');

Route::post('/update-user{id}')->name('update')->uses('UserController@updateUser');

Route::get('/update-user{id}')->uses('UserController@showUpdate');

Route::get('/delete-user{id}')->name('delete')->uses('UserController@deleteUser');