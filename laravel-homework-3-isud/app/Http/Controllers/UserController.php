<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function showForm()
	{
		return view('users');
	}

	public function getUsers(Request $request)
	{
		// $users = \DB::connection('mysql')->select('SELECT * FROM Users');

		//$users = \DB::table('Users')->paginate(3);
		
		$users = \DB::table('Users')->simplePaginate(3);

		return view('dashboard', ['users' => $users]);
	}

	public function insertUser(Request $request)
	{
		$username = $request->get('username');
		$vote = $request->get('vote');
		$is_active = $request->get('is_active');

		$successful = \DB::insert('INSERT INTO Users(name,votes,is_active) VALUES (?,?,?)', [$username,$vote,$is_active]);

		return redirect()->to('/get-users');
	}

	public function showUpdate(Request $request, $id)
	{
		$users = \DB::connection('mysql')->select('SELECT * FROM Users WHERE id=?',[$id]);

		return view('update',['users' => $users]);
		
	}

	public function updateUser(Request $request, $id)
	{
		$username = $request->get('username');
		$vote = $request->get('vote');
		$is_active = $request->get('is_active');


		\DB::table('Users')
            ->where('id', $id)
            ->update(['name' => $username, 'votes' => $vote, 'is_active' => $is_active]);

		return redirect()->to('/get-users');

	}

	public function deleteUser(Request $request, $id)
	{
		\DB::delete('DELETE FROM Users WHERE id = ?', [$id]);

		return redirect()->to('/get-users');
	}
}

