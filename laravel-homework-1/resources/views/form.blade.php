<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<title>Validacija</title>

	<style type="text/css">
		.submit:hover{
			background: black;
			color: white;
			font-weight: bold;
		}

	</style>
</head>
<body>
<div class="container">

	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h1 style="font-size: 32px" class="text-center">Форма за регистрација</h1>
		</div>
	</div>
	<div class="row">
	 	<div class="col-md-4 col-md-offset-4" style="margin-top: 25px">
			<form  class="form-horizontal" role="form" action="{{route('register')}}" method="POST">
		 	{{ csrf_field() }}
			<label>Внесете го Вашето корисничко име</label>
			<input type="text" name="username" class="form-control">

			<label>Внесете го Вашиот емаил</label>
			<input type="email" name="email" class="form-control">

			<label>Внесете го Вашиот датум на раѓање</label>
			<input type="date" name="date" class="form-control">

			<label>Внесете го Вашиот пасворд</label>
			<input type="password" name="password" class="form-control">
			<br>
			<input type="submit" value="Потврди" class="form-control submit">

			</form>
		</div>
	</div>		
</body>
</html>

