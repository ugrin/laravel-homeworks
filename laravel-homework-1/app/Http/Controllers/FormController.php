<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class FormController extends Controller
{
	public function viewForm()
	{
		return view('form');
	}

	private function checkUsername($username)
	{	
		$flag = false;
		for ($i=0; $i < strlen($username); $i++) 
		{
			if(ctype_alpha($username[$i]) == false && ctype_digit($username[$i]) == false)
			{
				$flag = true; break;
			}
		}

		return $flag;
	}

	private function checkEmail($email)
	{
		$email = explode('@',$email);
		if($email[1] == 'gmail.com' || $email[1] == 'yahoo.com')
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private function checkDate($date)
	{
		$date = explode('-', $date);

		if(2018-$date[0] > 18)
		{
			return true;
		}else
		{
			return false;
		}
	}

	private function checkPassword($password)
	{
		if(strlen($password) >= 8)
		{
			for ($i=0; $i < strlen($password) ; $i++) 
			{ 
				if($password[$i] == strtoupper($password[$i]))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}

	public function check(Request $request)
	{	
		$check1 = $this->checkUsername($request->get('username'));
		$check2 = $this->checkDate($request->get('date'));
		$check3 = $this->checkEmail($request->get('email'));
		$check4 = $this->checkPassword($request->get('password'));

		if($check1 && $check2 && $check3 && $check4)
		{
			echo "Добредојде  " . $request->get('username');
		}
		else
		{
			echo "Неуспешна валидација";
		}
	}
}