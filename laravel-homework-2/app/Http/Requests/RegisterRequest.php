<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
	public function rules()
	{
		$rules = [
			'username' => 'required|string|regex:/^(?=.*[_\W]).+$/',
			'email' => 'required|email|domain:gmail.com,hotmail.com,yahoo.com',
			'date' => 'required|date|before: 18 years ago',
			'password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
		];

		return $rules;
	}

	public function authorize()
	{
		return true;
	}

	public function messages()
	{
		$arr = [
			'username.regex' => 'Username must contain one special character.',
			'email.domain' => 'The domain of email must be gmail.com, yahoo.com or hotmail.com.',
			'date.before' => 'You must be at least 18 years old.',
			'password.min' => '',
			'password.regex' => 'Password must be at least 8 characters and contain one uppercase.'
		];

		return $arr;
	}
}
